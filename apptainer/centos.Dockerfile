FROM gitlab-registry.cern.ch/lhcb-docker/python-deployment:centos7
ARG HEP_OSLIBS_NAME=HEP_OSlibs

RUN yum update -y && \
    yum install -y epel-release && \
    yum install -y apptainer ${HEP_OSLIBS_NAME} && \
    yum clean all && \
    rm -rf /var/lib/apt/lists/* /lib/modules/* /lib/firmware/* /lib/kbd /var/cache/yum
