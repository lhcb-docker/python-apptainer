FROM gitlab-registry.cern.ch/lhcb-docker/python-deployment:python-3.9

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get install -y wget && \
    cd /tmp && \
    wget https://github.com/apptainer/apptainer/releases/download/v1.1.2/apptainer_1.1.2_amd64.deb && \
    apt-get install -y ./apptainer_1.1.2_amd64.deb && \
    rm -rf /var/lib/apt/lists/* /usr/share/man/man1
